/*
    Lorsque le navigatur a fini de construire le DOM
    Un événement "DOMContentLoaded" est envoyé
    Le code qui suit permet d'"écouter" cet événement
    afin lancer une fonction lorsqu'il se produit
*/
document.addEventListener('DOMContentLoaded', function () {
    const domCmdMainMenu = document.getElementById('cmd-main-menu');
    const domNavMainMenu = document.getElementById('nav-main-menu');

    const domCmdSkillsMenu = document.getElementById('cmd-skills-menu');
    const domCmdProjobsMenu = document.getElementById('cmd-projobs-menu');
    const domCmdDiplomasMenu = document.getElementById('cmd-diplomas-menu');

    const domCmdSkillsDiv = document.getElementById('cmd-skills-div');
    const domCmdProjobsDiv = document.getElementById('cmd-projobs-div');
    const domCmdDiplomasDiv = document.getElementById('cmd-diplomas-div');

    const domDDSkills = document.getElementById('skills-ul');
    const domDDProjobs = document.getElementById('projobs-ul');
    const domDDDiplomas = document.getElementById('diplomas-ul');




    domCmdMainMenu.addEventListener('click', function () {
        /*
        Est-ce que le menu est déjà caché ?
        - Oui => Afficher le menu et passer le bouton en croix
        - Non => Cacher le menu et  passer le bouton en burger
        */
        // Menu caché = l'élément possède la class "hidden"
        let isMenuHidden = domNavMainMenu.classList.contains('hidden');

        if (isMenuHidden) {
            domNavMainMenu.classList.remove('hidden');
            domCmdMainMenu.classList.add('close');
        }
        // else: "sinon", si la condition du "if" est false
        else {
            domNavMainMenu.classList.add('hidden');
            domCmdMainMenu.classList.remove('close');
        }
    });

    // Fermer le menu nav quand on clique sur un lien
    domNavMainMenu.addEventListener('click', function () {
        domNavMainMenu.classList.add('hidden');
        domCmdMainMenu.classList.remove('close');
    });


    // dropdown compétences
    domCmdSkillsMenu.addEventListener('click', function(){
        Dropdown(domCmdSkillsDiv, domDDSkills);
    });


    // dropdown experiences pros
    domCmdProjobsMenu.addEventListener('click', function () {
        Dropdown(domCmdProjobsDiv, domDDProjobs);
    });


    // dropdown diplômes
    domCmdDiplomasMenu.addEventListener('click', function () {
        Dropdown(domCmdDiplomasDiv, domDDDiplomas);
    });



    /* 
        TODO: EventListeners pour dérouler le contenu des dropdowns quand on arrive dessus via un intralien
    */



    function Dropdown(div, ul) {

        let isMenuHidden = ul.classList.contains('hidden');

        if (isMenuHidden) {
            div.classList.remove('closed');
            ul.classList.remove('hidden');
            div.classList.add('opened');
        }

        else {
            div.classList.remove('opened');
            ul.classList.add('hidden');
            div.classList.add('closed');
        }
    }

});